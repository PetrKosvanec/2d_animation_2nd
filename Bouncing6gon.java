import java.awt.*;

public class Bouncing6gon {
    int x;
    int y;
    Color color;
    int xDirection = 0;
    int yDirection = 0;
    final int SIDE = 20;

    /**
     * Initialize a new hexagon with its center located at (startX, startY), filled
     * with startColor.
     */
    public Bouncing6gon(int startX, int startY, Color startColor) {

        x = startX;
        y = startY;
        color = startColor;

    }

    /** Draws the hexagon at its current position on to surface. */
    public void draw(Graphics surface) {
        int x0 = x - SIDE/2;
        assert x0 > 0;
        int x1 = x0 + SIDE;
        int x2 = (int) (x1 + SIDE*Math.cos(Math.PI/3));
        int x3 = x1;
        int x4 = x0;
        int x5 = (int) (x0 - SIDE*Math.cos(Math.PI/3));

        int y0 = (int) (y - SIDE*Math.sin(Math.PI/3));
        int y1 = y0;
        int y2 = y;
        int y3 = (int) (y + SIDE*Math.sin(Math.PI/3));
        int y4 = y3;
        int y5 = y2;

        int[] xPoints = {x0, x1, x2, x3, x4, x5};
        int[] yPoints = {y0, y1, y2, y3, y4, y5};

// Draw the object
        surface.setColor( color );
        surface.fillPolygon( xPoints, yPoints, 6 );

        surface.setColor(Color.BLACK);
        ((Graphics2D) surface).setStroke(new BasicStroke(3.0f));

        surface.drawPolygon( xPoints, yPoints, 6 );

// Move the center of the object each time we draw it
        x += xDirection;
        y += yDirection;

// If we have hit the edge and are moving in the wrong direction, reverse direction
// We check the direction because if a box is placed near the wall, we would get "stuck"
// rather than moving in the right direction
        if ((x - (SIDE/2 + SIDE*Math.cos(Math.PI/3)) <= 0 && xDirection < 0) ||

                (x + SIDE/2 + SIDE*Math.cos(Math.PI/3) >= SimpleDraw.WIDTH && xDirection > 0)) {
            xDirection = -xDirection;
        }

        if ((y - SIDE*Math.sin(Math.PI/3) <= 0 && yDirection < 0) ||

                (y + SIDE*Math.sin(Math.PI/3) >= SimpleDraw.HEIGHT && yDirection > 0)) {
            yDirection = -yDirection;
        }
    }

    public void setMovementVector(int xIncrement, int yIncrement) {
        xDirection = xIncrement;
        yDirection = yIncrement;
    }
}


