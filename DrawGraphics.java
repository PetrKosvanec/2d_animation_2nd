//  	 Oval is not enough, I prepared ...three-pointed STAR ... ADD IT here.


import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

public class DrawGraphics {

    Bouncer movingSpriteB;
    StraightMover movingSpriteSM;


    private ArrayList<Mover> moversArrayList = new ArrayList<>();

    Color[] colors = new Color[6];  // array of colors;

    /**
     * Initializes this class for drawing.
     */
    // 3 colors, 3 directions -> do it in one loop
    public DrawGraphics() {

        colors[0] = new Color( 228, 132, 0, 255 );     // fulvous
        colors[1] = new Color( 255, 196, 12 );            // mikado yellow
        colors[2] = new Color( 132,25,8 );               // falu red
        colors[3] = new Color( 107, 126, 115 );          // xanadu grey
        colors[4] = new Color( 208,164,00 );
        colors[5] = new Color( 100, 84, 82 );          // wenge gray

        for (int i=0; i < 6; i++) {
            colors.add(colors[i]);
        }

        Oval oval = new Oval(25, 49, c1);
        movingSpriteB = new Bouncer(200, 20, oval);
        movingSpriteSM = new StraightMover(255, 200, oval);
        movingSpriteB.setMovementVector(1, 3);
        movingSpriteSM.setMovementVector(-2, -1);

        moversArrayList.add(movingSpriteSM);
        moversArrayList.add(movingSpriteB);

        Rectangle box = new Rectangle(15, 20, Color.RED);
        Color c2 = new Color(0, 255, 0, 225);
        Rectangle box2 = new Rectangle(50, 10, c2);

        movingSpriteB = new Bouncer(230, 200, box2);
        movingSpriteB.setMovementVector(2, 3);
        moversArrayList.add(movingSpriteB);

        movingSpriteSM = new StraightMover(155, 260, box);
        movingSpriteSM.setMovementVector(-1, -1);
        moversArrayList.add(movingSpriteSM);
    }

    /**
     * Draw the contents of the window on surface.
     */
    public void draw(Graphics surface) {
        for (Mover mov : moversArrayList) {
            mov.draw(surface);
        }
    }
}