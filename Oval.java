import java.awt.*;

public class Oval implements Sprite {

    private int width;
    private int height;
    private Color color;

    public Oval(int width, int height, Color color) {

        this.width = width;
        this.height = height;
        this.color = color;
    }

    // draw method
    @Override
    public void draw(Graphics surface, int leftX, int topY) {
        surface.setColor( color );
        surface.fillOval( leftX, topY, width, height );         // fillOval(int x, int y, int width, int height)
        surface.setColor( Color.BLACK );
        ((Graphics2D) surface ).setStroke(new BasicStroke (3.0f));
        surface.drawOval( leftX, topY, width, height );         // drawOval(dtto) - obrys

    }

    public int getWidth() { return width; }

    public int getHeight() { return height; }
}
